# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

ARG PROJECT_NAME
ARG PROJECT_VERSION
ARG PROJECT_URL

ARG BASE_IMAGE="ubuntu:19.04"

ARG WORK_DIR="/work"
ARG LABELS_FILE="/.labels"

FROM "${BASE_IMAGE}"

	ARG PROJECT_NAME
	ARG PROJECT_VERSION
	ARG PROJECT_URL

	ARG BASE_IMAGE

	ARG LABELS_FILE
	ARG WORK_DIR

	RUN \
		set -e; \
		set -u; \
		apt-get update; \
		apt-get install --yes --quiet \
			"apt-utils=1.8.1"; \
		apt-get install --yes --quiet \
			"autoconf=2.69-11" \
			"binutils=2.32-7ubuntu4" \
			"bison=2:3.3.2.dfsg-1" \
			"bzip2=1.0.6-9ubuntu0.19.04.1" \
			"ca-certificates=20190110" \
			"cmake=3.13.4-1" \
			"curl=7.64.0-2ubuntu1.1" \
			"doxygen=1.8.13-10ubuntu1" \
			"expat=2.2.6-1" \
			"file=1:5.35-4" \
			"flex=2.6.4-6.2" \
			"g++-8=8.3.0-6ubuntu1" \
			"gawk=1:4.2.1+dfsg-1build1" \
			"gcc=4:8.3.0-1ubuntu3" \
			"gdb=8.2.91.20190405-0ubuntu3" \
			"git=1:2.20.1-2ubuntu1" \
			"golang=2:1.10~4ubuntu1" \
			"iproute2=4.18.0-1ubuntu2" \
			"less=487-0.1build1" \
			"libtool=2.4.6-10" \
			"lua5.3=5.3.3-1.1ubuntu1" \
			"lzip=1.21-3" \
			"m4=1.4.18-2" \
			"make=4.2.1-1.2" \
			"net-tools=1.60+git20180626.aebd88e-1ubuntu1" \
			"openssh-client=1:7.9p1-10" \
			"openssl=1.1.1b-1ubuntu2.1" \
			"patch=2.7.6-3" \
			"pkg-config=0.29.1-0ubuntu2" \
			"perl=5.28.1-6" \
			"python2=2.7.16-1" \
			"python3=3.7.3-1" \
			"ruby=1:2.5.1" \
			"screen=4.6.2-3" \
			"sudo=1.8.27-1ubuntu1" \
			"tree=1.8.0-1" \
			"valgrind=1:3.14.0-2ubuntu6" \
			"vim=2:8.1.0320-1ubuntu3.1"; \
		mkdir -p "${WORK_DIR}"; \
		chmod 1777 "${WORK_DIR}"; \
		labels_dir="${LABELS_FILE%/*}"; \
		test "${labels_dir}" && mkdir -p "${labels_dir}"; \
		echo "project.name=${PROJECT_NAME}" >> "${LABELS_FILE}"; \
		echo "project.version=${PROJECT_VERSION}" >> "${LABELS_FILE}"; \
		echo "project.url=${PROJECT_URL}" >> "${LABELS_FILE}"; \
		echo "base_image=${BASE_IMAGE}" >> "${LABELS_FILE}"; \
		echo "work_dir=${WORK_DIR}" >> "${LABELS_FILE}"; \
		echo "labels.file=${LABELS_FILE}" >> "${LABELS_FILE}"; \
		dpkg --list| \
		while IFS="" read -r _line; \
		do \
			case "${_line}" in \
			"ii  "*) \
				_line="${_line#ii  }"; \
				_package="${_line%% *}"; \
				_line="${_line#${_package}}"; \
				_prefix="${_line%%[^ ]*}"; \
				_line="${_line#${_prefix}}"; \
				_version="${_line%% *}";\
				echo "package.${_package}=${_version}" >> "${LABELS_FILE}"; \
			;; \
			esac; \
		done; \
		return;

	WORKDIR "${WORK_DIR}"

	LABEL "labels.file" "${LABELS_FILE}"
