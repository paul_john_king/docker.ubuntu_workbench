#!/bin/sh

# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

# TODO: Give a project URL that clones the commit, not the general project.

# TODO: Add an option to push the image (and possibly login) to the registry.
# This will require reworking some of the variables.

# TODO: See if I can use the `--label` option in a way that avoids so many
# intermediate images.

# TODO: Convert this to a generic, parameterisable script that can be used to
# build any number of labelled images.

# TODO: If this becomes generic, allow for a default, empty Docker registry for
# use by those that are not going to push their image.

# TODO: If this becomes generic, port to Go and compile for multiple
# environments.

set -e;
set -u;

_main(){
	local _PROJECT_NAME="docker.ubuntu_workbench";
	local _GIT_REGISTRY="https://gitlab.com/paul_john_king";
	local _DOCKER_REGISTRY="registry.gitlab.com/paul_john_king";

	local _project_description;
	local _project_version;
	local _project_url;
	local _docker_alias;
	local _stage;
	local _label_args
	local _labels_file;
	local _labels;
	local _label;

	_project_description=$(git describe --long --dirty);
	_project_version="${_project_description%%-*}";
	_project_description="${_project_description#*-}";
	_project_version="${_project_version}.${_project_description%%-*}";
	_project_description="${_project_description#*-g}";
	_project_version="${_project_version}_${_project_description}";
	if test "${_project_version}" != "${_project_version%-dirty}";
	then
		_project_version="${_project_version%-dirty}_dirty";
	fi;

	_project_url="${_GIT_REGISTRY}/${_PROJECT_NAME}";

	_docker_alias="${_DOCKER_REGISTRY}/${_PROJECT_NAME}:${_project_version}";

	for _stage in "Building fresh image" "Labelling built image";
	do
		echo "${_stage}";
		_label_args="";
		_labels_file=$(
			docker image inspect \
				--format '{{index .ContainerConfig.Labels "labels.file"}}' \
				"${_docker_alias}" 2>"/dev/null" || true;
		);
		if test "${_labels_file}";
		then
			_labels=$(
				docker run \
					--rm \
					"${_docker_alias}" \
					cat "${_labels_file}" 2>"/dev/null" || true;
			);
			if test "${_labels}";
			then
				_label_args=$(
					echo "${_labels}"|
					while IFS="" read -r _label;
					do
						echo "--label ${_label}";
					done;
				);
			fi;
		fi;
		docker build \
			--build-arg PROJECT_NAME="${_PROJECT_NAME}" \
			--build-arg PROJECT_VERSION="${_project_version}" \
			--build-arg PROJECT_URL="${_project_url}" \
			$(echo "${_label_args}") \
			--tag "${_docker_alias}" \
			"${@}" ".";
	done ;

	return 0;
}

_main "${@}";
